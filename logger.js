/** ***********************************************************************
*
* Veracity Technology Consultants 
* __________________
*
*  2020 Veracity Technology Consultants
*  All Rights Reserved.
*
*/

const colors = require('colors/safe');
const systemPlugin = require('./utils/plugins/systemPlugin.js');

//This demo shows how you can capture many types of system events. 

module.exports = class Logger extends systemPlugin {
    constructor(odm, settings) {
        super(odm, settings);

        if (settings.ui) {
            this.on('uiRequest', (e, req, res) => {

                this.prodLog(colors.cyan('Logger:') + colors.underline('UI'), colors.green(req.method), req.url, colors.red(req.user ? req.user.email : ''));
            });
        }
        if (settings.systemapi) {
            this.on('systemAPIRequest', (e, req, res) => {
                this.prodLog(colors.cyan('Logger:') + colors.underline('UI'), colors.green(req.method), req.url, colors.red(req.key ? req.key.name : ''));
            });
        }
        if (settings.lrsapi) {
            this.on('LRSAPIRequest', (e, req, res) => {
                this.prodLog(colors.cyan('Logger:') + colors.underline('UI'), colors.green(req.method), req.url, colors.red(req.key ? req.key.name : ''), req.lrs ? req.lrs.lrsName : '');
            });
        }
        if (settings.xapi) {
            this.on('xapiRequest', (e, req, res) => {
                this.prodLog(colors.cyan('Logger:') + colors.underline('XAPI'), colors.green(req.method), req.url, colors.red(req.lrs ? req.lrs.lrsName : ''));
            });
        }
        if (settings.systemodm) {
            this.on('systemODMEvent', (e, method, type, model) => {
                this.prodLog(colors.cyan('Logger:') + colors.underline('ODM'), colors.green(method), type, colors.red(model._id + ':' + model.uuid));
            });
        }
        if (settings.lrsodm) {
            this.on('LRSODMEvent', (e, method, type, lrs, model) => {
                this.prodLog(colors.cyan('Logger:') + colors.underline('LRSODM'), colors.green(method), type, colors.magenta(lrs), colors.red(model._id + ':' + model.uuid));
            });
        }
        this.on('systemStartup', (e) => {
            this.prodLog(colors.cyan('Logger:') + colors.underline('System'), colors.green('System Start'));
        });
        this.on('systemShutdown', (e) => {
            this.prodLog(colors.cyan('Logger:') + colors.underline('System'), colors.green('System Shutdown'));
        });
        this.on('systemError', (e, err) => {
            this.prodLog(colors.cyan('Logger:') + colors.underline('System'), colors.red('Error ' + err.message));
        });
    }
    prodLog(...m) {
        console.log((Date.now()) + ' ' + m.join(' '));
    }
    static get display() {
        return {
            title: 'Logging',
            description: 'A configurable logging plugin which writes to standard out',
        };
    }
    static get metadata() {
        return {
            author: 'Veracity Technology Consultants',
            version: '1.0.0',
            moreInfo: 'https://www.veracity.it',
        };
    }

    static get settingsForm() {
        return [

            {
                label: 'UI Requests',
                id: 'ui',
                helptext: 'Log requests to the web ui',
                type: { isCheck: true },
            },
            {
                label: 'xAPI Requests',
                id: 'xapi',
                helptext: 'Log requests to the xAPI',
                type: { isCheck: true },
            },
            {
                label: 'LRS API Requests',
                id: 'lrsapi',
                helptext: 'Log requests to the LRS API',
                type: { isCheck: true },
            },
            {
                label: 'System API',
                id: 'systemapi',
                helptext: 'Log requests to the System API',
                type: { isCheck: true },
            },
            {
                label: 'System ODM Events',
                id: 'systemodm',
                helptext: 'Log changes to system database models',
                type: { isCheck: true },
            },
            {
                label: 'LRS ODM Events',
                id: 'lrsodm',
                helptext: 'Log changes to LRS database models',
                type: { isCheck: true },
            },

        ];
    }
};
module.exports.pluginName = 'Logger';

