/** ***********************************************************************
*
* Veracity Technology Consultants 
* __________________
*
*  2020 Veracity Technology Consultants
*  All Rights Reserved.
*
*/

//This is a plugin dashboard. It contains 2 widgets that call on the same processor
class CustomDash extends Dashboard {
    constructor(params, db) {
        super(params, db);
    }
    //Below, we return the list of processors, and their parameters. Note that dashboard parameters will flow down to the processors, unless they are
    //replaced manually in the params field
    async getGraphs() {
        let graphs = [{
            handler: 'demoProcessor',
            params: {
                //This will inherit the values from the dashboard query
            },

        },
        {
            handler: 'demoProcessor',
            params: {
                activity:"http://demo.com" // This will replace the value that was sent to the dashboard.
            },

        }];

        //Dashboards can make decisions about what widgets to show. You can also return a promise or use an async function, in order
        //to make decisions about what dashboard to show based on a database. 
        if(this.param(activity) == "demoID")
        {
            graphs.push( {
                handler: 'ADifferentProcessor',
            })
        }
        return graphs;
    }
    static getConfiguration() {
        return {
            TTL: '60 minutes',
            //Tell the system what sort of parameters this dashboard expects, and how their pickers should be rendered
            parameters: {
                range: { title: "range", type: new TimeRange('Time Range', ''), required: false, default:"last90days" },
                activity: { title: "activity", type: new ActivityPicker('Activity', true, 'Which activity to graph?'), required: false },
                actor: { title: "actor", type: new ActorPicker('Activity', true, 'Which actor to graph?'), required: false },
            },
            title: 'Plugin Dashboard Demo',
            defaultLoaded: false,
        };
    }
}

module.exports = CustomDash;
