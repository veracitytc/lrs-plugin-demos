/** ***********************************************************************
*
* Veracity Technology Consultants 
* __________________
*
*  2020 Veracity Technology Consultants
*  All Rights Reserved.
*
*/

//This is a demo processor. The simplest form of processor is one that runs a MongoDB pipeline.

module.exports = class MyProcessor extends AnalyticProcessor {
    constructor(params, db, lrs) {
        super(params, db, lrs);


        this.pipeline = [
            //The common stage utility adds support for the global sanity data limit and/or the global time filter parameter
            ...CommonStages(this, {
                range: false,
                limit: true,
            }),
            {
                $match: {
                    'statement.object.id': this.param('activity'), //Use the provided activity parameter value. This can be inherited from the dashboard, 
                                                                   //or specified in the custom dashboard UI. 
                },
            },
            {
                $limit: 1000,
            },
            {
                $group: {
                    _id: '$statement.actor.id',
                    count: {
                        $sum: 1,
                    },
                },
            },
        ];

        //Chart configuration. Make a bar chart where the colums are set by _id, and the height is the value of "count"
        this.chartSetup = new BarChart('_id', 'count');

        //A JS side mapping function. This one maps the _id value to an actor display name.
        this.map = MapToActorNameAsync('_id');
    }
    map(val) {
        
        //Note that this is overridden in the constructor. You could also add logic here instead. 
        return val;
    }
    filter(val) {
        return Math.random() > 0.5; //Randomly choose results
    }
    exec(results) {
        return results; //Here in a final chance to do some logic JS side over the whole result set. 
    }
    static getConfiguration() {
        const conf = new ProcessorConfiguration('Demo', ProcessorConfiguration.widgetType.graph, ProcessorConfiguration.widgetSize.small);
        conf.addParameter('activity', new ActivityPicker('Activity', 'Choose the activity to plot'), true);
        return conf;
    }
};
