/** ***********************************************************************
*
* Veracity Technology Consultants 
* __________________
*
*  2019 Veracity Technology Consultants
*  All Rights Reserved.
*
*/

//This file is an overview of the various 
const systemPlugin = require('./utils/plugins/systemPlugin.js');
const express = require('express');

module.exports = class systemDemo extends systemPlugin {
    constructor(odm, settings) {
        super(odm, settings);

        // System plugins get all the same events as an LRS plugin, with the first param the value of WHICH lrs fired the event
        this.on('statementBatchStored', (event, lrs, batch) => {
            console.log(batch);
        });
        // They can also schedule future signals to themselves.
        this.onInterval('selfSignal', (event, data) => {
            console.log(data);
        });

        // Plugins can expose routes to the webserver
        this.router = express();
        // And use normal express middleware
        this.router.use((req, res, next) => {
            console.log('got here', req.url);
            next();
        });
        // This route is hooked to a menu item below.
        this.router.get('/schedule', (req, res, next) => {
            // When it is called, it schedules a signal to be sent to this LRS in 10 seconds.
            this.schedule('10 seconds', 'selfSignal', { string: 'This is a system level plugin timeout' });
            res.redirect('back');
        });
        // Another route example.
        this.router.get('/settings', (req, res, next) => {
            res.send(settings);
        });

        // This will be called every 10 seconds, because an interval was installed when the plugin was installed
        this.onInterval('10Int', async (e) => {
            // Here is a demo of persisting state in a plugin
            const state = await this.getState();
            if (!state.count) { state.count = 1; } else { state.count++; }
            await this.persistState(state);
            console.log('This system level plugin prints every 10 seconds, or the shortest interval. Ran ' + state.count + ' times.');
        });
    }
    // Install will be run when the plugin is added into an LRS
    async install() {
        console.log('install me ' + this.uuid);
        // Schedule the event "10Int" to be sent to this LRS every 10 seconds.
        this.every('10 seconds', '10Int');
    }
    // Uninstall will be run when the plugin is removed from the LRS
    async uninstall() {
        super.uninstall();
        console.log('system uninstall me ' + this.uuid);
    }
    // Metadata for display
    static get display() {
        return {
            title: 'Demo',
            description: 'A demo plugin loaded from the filesystem',
        };
    }
    // Additional metadata for display
    static get metadata() {
        return {
            author: 'Veracity Technology Consultants',
            version: '1.0.0',
            moreInfo: 'https://www.veracity.it',
        };
    }

    // This form will be presented to the user when they install or edit the plugin.
    // For each item in the array, the settings object will contain a property who's name is the 'id' of the entry
    // and the type is determined by the form component type. The below form would yield a settings object like
    // {
    //    nameOfProperty:"SomeString"
    //    checkbox:true,
    //    select:"the selected value",
    // }
    static get settingsForm() {
        return [
            {
                label: 'String with client side validation',
                id: 'nameOfProperty',
                helptext: 'User should type a string',
                validation: "val !== undefined && val !== '' && val.length > 2 && val.length < 100",
                validationMessage: 'Enter a string',
                placeholder: 'Show this as the place holder',
                type: { isText: true, type: 'text' },
            },
            {
                label: 'Checkbox',
                id: 'checkbox',
                helptext: 'This is either true or false',
                type: { isCheck: true },
            },
            {
                label: 'A Select',
                id: 'select',
                helptext: 'What should you pick',
                type:
                { isSelect: true },
                options: [
                    {
                        text: 'I attempted it',
                        value: 'http://adlnet.gov/expapi/verbs/attempted',
                    },
                    {
                        text: 'I attended it',
                        value: 'http://adlnet.gov/expapi/verbs/attended',
                    },
                ],
            },
        ];
    }
};
module.exports.pluginName = 'systemDemo';
