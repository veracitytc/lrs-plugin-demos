/** ***********************************************************************
*
* Veracity Technology Consultants 
* __________________
*
*  2020 Veracity Technology Consultants
*  All Rights Reserved.
*
*/

// There is a bit of magic in the require call for a plugin.
// You can choose from a whitelist of internal system objects.
// If the required file is not in that list, it will be required from disk.
const lrsPlugin = require('./utils/plugins/lrsPlugin.js');

// When requiring a node_module, if the compiled LRS uses that module, it will be returned from the compiled bundle.
// Otherwise, you'll need to make sure to npm install it alongside the plugin.

const express = require('express');
const request = require('request');

module.exports = class demo extends lrsPlugin {
    constructor(lrs, dal, settings) {
        super(lrs, dal, settings);

        // Plugins can catch various events that happen in the LRS
        //This particular event is an important one. From here, you can fire off the data to other systems.
        this.on('statementBatchStored',  (event, batch) => {
            console.log(batch);

            //Events can return promises! Note that the logic flow that initially fired this event
            //will wait for the promise to resolve

            //Note the below is notional and not tested. This shows how you could HTTP Post the data out to another server.
            //There are libraries available in NPM for other databases and transports.
            return new Promise( resolve =>{
                request.post({
                    url:"www.data.com/import",
                    json:batch.map(i => i.statement),
                    callback:(err,res,body) =>{
                        resolve();
                    }
                })
            })

        });
        // They can also schedule future signals to themselves.
        this.onInterval('selfSignal', (event, data) => {
            console.log(data);
        });

        // Plugins can expose routes to the webserver
        this.router = express();
        // And use normal express middleware
        this.router.use((req, res, next) => {
            console.log('got here', req.url);
            next();
        });
        // This route is hooked to a menu item below.
        this.router.get('/schedule', (req, res, next) => {
            // When it is called, it schedules a signal to be sent to this LRS in 10 seconds.
            this.schedule('10 seconds', 'selfSignal', { string: 'value' });
            res.redirect('back');
        });
        // Another route example.
        this.router.get('/settings', (req, res, next) => {
            res.send(settings);
        });

        // This will be called every 10 seconds, because an interval was installed when the plugin was installed
        this.onInterval('10Int', async (e) => {
            // Here is a demo of persisting state in a plugin
            const state = await this.getState();
            if (!state.count) { state.count = 1; } else { state.count++; }
            await this.persistState(state);
            console.log('This prints every 10 seconds, or the shortest interval. Ran ' + state.count + ' times.');
        });
        this.on('portalSection', () => ({
            title: 'View Instructor Dashboards',
            body: `
                  <p>This plugin allows you to view dashboards for specific classes that you manage.</p>
                  <a href="${this.getLink('/settings/')}"> <div class="btn btn-raised btn-primary"  >Settings</div> </a>
                `,
        }));
        this.on('lrsSidebar', (event, lrs) =>
            ({
                text: 'LRS Sidebar',
                href: '#' + this.uuid + '2',
                id: this.uuid + '2',
                children: [
                    {
                        text: 'settings',
                        href: this.getLink('/settings'),
                    },
                    {
                        text: 'schedule',
                        href: this.getLink('/schedule'),
                    },
                ],
            }));
    }
    // Install will be run when the plugin is added into an LRS
    async install() {
        console.log('install me ' + this.uuid);
        // Schedule the event "10Int" to be sent to this LRS every 10 seconds.
        this.every('10 seconds', '10Int');
    }
    // Uninstall will be run when the plugin is removed from the LRS
    async uninstall() {
        console.log('uninstall me ' + this.uuid);
        // We need to make sure to call the superclass uninstall, or we'll leave behind queued events!
        super.uninstall();
    }
    // Metadata for display
    static get display() {
        return {
            title: 'Demo',
            description: 'A demo plugin loaded from the filesystem',
        };
    }
    // Additional metadata for display
    static get metadata() {
        return {
            author: 'Veracity Technology Consultants',
            version: '1.0.0',
            moreInfo: 'https://www.veracity.it',
        };
    }

    // This form will be presented to the user when they install or edit the plugin.
    // For each item in the array, the settings object will contain a property who's name is the 'id' of the entry
    // and the type is determined by the form component type. The below form would yield a settings object like
    // {
    //    nameOfProperty:"SomeString"
    //    checkbox:true,
    //    select:"the selected value",
    // }
    static get settingsForm() {
        return [
            {
                label: 'String with client side validation',
                id: 'nameOfProperty',
                helptext: 'User should type a string',
                validation: "val !== undefined && val !== '' && val.length > 2 && val.length < 100",
                validationMessage: 'Enter a string',
                placeholder: 'Show this as the place holder',
                type: { isText: true, type: 'text' },
            },
            {
                label: 'Checkbox',
                id: 'checkbox',
                helptext: 'This is either true or false',
                type: { isCheck: true },
            },
            {
                label: 'A Select',
                id: 'select',
                helptext: 'What should you pick',
                type:
                { isSelect: true },
                options: [
                    {
                        text: 'I attempted it',
                        value: 'http://adlnet.gov/expapi/verbs/attempted',
                    },
                    {
                        text: 'I attended it',
                        value: 'http://adlnet.gov/expapi/verbs/attended',
                    },
                ],
            },
        ];
    }
};


module.exports.pluginName = 'demo';
