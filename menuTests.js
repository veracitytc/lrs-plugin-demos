/** ***********************************************************************
*
* Veracity Technology Consultants
* __________________
*
*  2020 Veracity Technology Consultants
*  All Rights Reserved.
*/
const systemPlugin = require('./utils/plugins/systemPlugin.js');
const express = require('express');

// This shows some of the system level UI events you can hook into. Each example below shows how you can add elements to different parts of the 
// interface
module.exports = class menuAugment extends systemPlugin {
    constructor(odm, settings) {
        super(odm, settings);

        this.on('adminTools', (event) => ({
            text: 'Admin Tool Demo',
            href: this.getLink('/test'),
            subtext: 'This is where the subtext goes',
        }));

        this.on('lrsHomeLinks', (event, lrs) => ({
            text: 'xAPI Home Menu',
            href: this.getLink('/test'),
            subtext: 'This is where the subtext goes',
        }));

        this.on('lrsSidebar', (event, lrs) => ({
            text: 'System Plugin',
            href: this.getLink('/lrsSidebar'),
            id: this.uuid,
        }));
        this.on('lrsSidebar', (event, lrs) => (
            {
                text: 'System Plugin Children',
                href: '#' + this.uuid + '2',
                id: this.uuid + '2',
                children: [
                    {
                        text: 'one',
                        href: this.getLink('/lrsSidebar/one'),
                    },
                    {
                        text: 'one',
                        href: this.getLink('/lrsSidebar/one'),
                    },
                ],
            }));

        this.on('head', () => '<title>Changed</title>');


        this.on('xapiTools', (event) => ({
            text: 'xAPI Tool Demo',
            href: this.getLink('/test'),
            subtext: 'This is where the subtext goes',
        }));

        this.router = express();

        // This route is hooked to a menu item below.
        this.router.get('/test', (req, res, next) => {
            res._render('test');
        });
        // Plugins can catch various events that happen in the LRS
        this.on('headerMenu', (event) => ({
            text: 'Menu Demo',
            href: this.getLink('/test'),
            children: [{
                text: 'New sub Item',
                href: this.getLink('/test'),

            }, {
                text: 'New sub Item',
                href: this.getLink('/test'),
                children: [{
                    text: 'New sub sub Item',
                    href: this.getLink('/test'),

                }],
            }, {
                text: 'New sub Item',
                href: this.getLink('/test'),
                children: [{
                    text: 'New sub sub Item',
                    href: this.getLink('/test'),

                },
                {
                    text: 'New sub sub Item',
                    href: this.getLink('/test'),

                }],
            }],
        }));
    }

    static get display() {
        return {
            title: 'Menu Augment',
            description: 'A demo plugin for adding UI elements',
        };
    }
    // Additional metadata for display
    static get metadata() {
        return {
            author: 'Veracity Technology Consultants',
            version: '1.0.0',
            moreInfo: 'https://www.veracity.it',
        };
    }

    static get settingsForm() {
        return [

        ];
    }
};

module.exports.pluginName = 'menuAugment';
